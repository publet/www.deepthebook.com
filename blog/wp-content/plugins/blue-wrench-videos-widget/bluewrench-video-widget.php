<?php
/*
Plugin Name:  Blue Wrench Video Widget
Plugin URI: http://bluewrenchsoftware.com/wordpress-plugins/bluewrench-video-widget/
Description: Blue Wrench Video Widget to display videos from various video sharing networks such as Vimeo, YouTube, Metacafe etc into your widget box. Simply copy the video's URL from your web browser's address bar and paste it in 'Add new video' page and add 'Blue Wrench Video Widget' from WP-Admin >> Appearance >> Widgets to desired area. Supported Networks: Vimeo, YouTube, BlipTV, Dailymotion, Veoh, Metacafe, MeFeedia and Break
Version: 2.0.1
Author: Sunil Nanda
Author URI: http://www.sunilnanda.com/

*/

$siteurl = get_option('siteurl');
define('BW_FOLDER', dirname(plugin_basename(__FILE__)));
define('BW_URL', $siteurl.'/wp-content/plugins/' . BW_FOLDER);
define('BW_FILE_PATH', dirname(__FILE__));
define('BW_DIR_NAME', basename(BW_FILE_PATH));
if (!defined('BW_VERSION_NUM'))
	define('BW_VERSION_NUM', '2.0.1');
define('BW_DEV_MODE', true);

global $wpdb;
$bw_table_prefix=$wpdb->prefix.'bw_';
define('BW_TABLE_PREFIX', $bw_table_prefix);

function bw_install(){
    global $wpdb;
    $table = BW_TABLE_PREFIX."videos";
    $structure = "CREATE TABLE $table (
		id int(9) NOT NULL AUTO_INCREMENT,
		title varchar(150) NOT NULL,
		value varchar(255) NOT NULL,
		sortorder int(3) NOT NULL DEFAULT  '9999',
		dateadded datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		post_status varchar(20) NOT NULL DEFAULT 'publish',
		UNIQUE KEY id (id)
    );";
    $wpdb->query($structure);
	add_option("Blue Wrench Video Widget Version", BW_VERSION_NUM);
}
function bw_uninstall(){
    global $wpdb;
    $table = BW_TABLE_PREFIX."videos";
    $structure = "drop table if exists $table";
    $wpdb->query($structure);  
	delete_option("Blue Wrench Video Widget Version", BW_VERSION_NUM);
}
function bw_init_method(){
	if (is_admin()) {
		wp_enqueue_script('jquery');
		wp_register_script( 'bw_ajax_script', plugins_url( '/bw_script.js', __FILE__ ), array('jquery'));
		wp_localize_script( 'bw_ajax_script', 'bwAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));
		wp_enqueue_script( 'bw_ajax_script' );
		wp_register_style ( 'bw-plugin-jquery-ui', plugin_dir_url( __FILE__ ) . 'jquery-ui.css' );
		wp_enqueue_style( 'bw-plugin-jquery-ui' );
	}
}
function bw_admin_menu() {  
	add_menu_page(
		'BW Videos',
		'BW Videos',
		'edit_posts',
		__FILE__,
		'bw_video_controller',
		BW_URL."/images/video_icon.png",25
	);
	add_submenu_page(__FILE__,'Add new video','Add new video','8','bw-videos','bw_admin_add_new');
}
require_once(dirname(__FILE__) . "/class-bluewrench-video-controller.php");

function bw_video_controller(){
	//require_once(dirname(__FILE__) . "/admin-list-site.php");
	require_once(dirname(__FILE__) . "/bluewrench-video-controller.php");
}
function bw_admin_add_new(){
	//require_once(dirname(__FILE__) . "/admin-videos.php");
	require_once(dirname(__FILE__) . "/bluewrench-video-controller.php");
}

// Video Widget
require_once(dirname(__FILE__) . "/class-bluewrench-video-widget.php");

function load_bw_video_widgets(){
	wp_register_style ( 'bw-plugin-css', plugin_dir_url( __FILE__ ) . 'bluewrench-video-widget.css' );
	wp_enqueue_style ( 'bw-plugin-css' );
	register_widget('BlueWrenchVideoWidget');

}

function fetch_video_embedd_html(){
	if (isset($_REQUEST['vid']) && $_REQUEST['vid']!=""){
		$vid = $_REQUEST['vid'];
		global $wpdb;
		$sql = "SELECT * FROM ".BW_TABLE_PREFIX."videos where  id='".$vid."' limit 1";
		$results = $wpdb->get_results($sql);
		if (is_array($results) && count($results)>0){
			$bwMediaController = new BlueWrenchMediaController();
			$video_row = array_pop($results);
			$video_url = $video_row->value;
			$video_preview = $bwMediaController->generateEmbeddHTML($video_url);
			//bw_filter_allowed_http_origins();
			echo $video_preview;
			die();
		}
	}
}

register_activation_hook(__FILE__,'bw_install');
register_deactivation_hook(__FILE__ , 'bw_uninstall' );
add_action('admin_init', 'bw_init_method');
add_action('admin_menu', 'bw_admin_menu');
add_action('widgets_init', 'load_bw_video_widgets');		//action to initiate widgets
add_action('wp_ajax_fetch_video_embedd_html', 'fetch_video_embedd_html'); // Call when user logged in
	
?>