<?php
	wp_reset_vars(array('action', 'id'));
	$columns = array(
		'id' => 'ID',
		'title' => 'Title',		
		'sortorder' => 'Sort Order',		
		'dateadded' => 'Date',		
		'action' => 'Action'		
	);
	global $bw_error_messages;
	$bw_error_messages = array(
		-1 => 'Please provide a valid video URL.',
		-2 => 'Video URL already found in the database.',
		-3 => 'An internal error occurred.',
		-4 => 'This video network is not yet supported. Please contact the author of this plugin <a href="http://www.sunilnanda.com/contact/" target="_blank">here</a> for more help.'
	);

	register_column_headers('bw-listing', $columns);	
	if (!function_exists("p")){
		function p($obj){
			echo "<pre>";
			print_r($obj);
			echo "</pre>";
		}
	}
	function bw_perform($table, $data, $action = 'insert', $parameters = '') {
		reset($data);
		global $wpdb;
		if ($action == 'insert') {
			$query = 'insert into ' . $table . ' (';
			while (list($columns, ) = each($data)) {
				$query .= $columns . ', ';
			}
			$query = substr($query, 0, -2) . ') values (';
			reset($data);
			while (list(, $value) = each($data)) {
				switch ((string)$value) {
					case 'now()':
						$query .= 'now(), ';
						break;
					case 'null':
						$query .= 'null, ';
						break;
					default:
						$query .= '\'' . bw_input($value) . '\', ';
						break;
				}
			}
			$query = substr($query, 0, -2) . ')';
		} elseif ($action == 'update') {
			$query = 'update ' . $table . ' set ';
			while (list($columns, $value) = each($data)) {
				switch ((string)$value) {
					case 'now()':
						$query .= $columns . ' = now(), ';
						break;
					case 'null':
						$query .= $columns .= ' = null, ';
						break;
					default:
						$query .= $columns . ' = \'' . bw_input($value) . '\', ';
						break;
				}
			}
			$query = substr($query, 0, -2) . ' where ' . $parameters;
		}
		return $wpdb->query($query);
	}
	function bw_input($string) {
		return addslashes($string);
	}
	function bw_prepare_input($string) {
		if (is_string($string)) {
			return trim(stripslashes($string));
		} elseif (is_array($string)) {
			reset($string);
			while (list($key, $value) = each($string)) {
				$string[$key] = bw_prepare_input($value);
			}
			return $string;
		} else {
			return $string;
		}
	}
	function bw_set_status($id=null, $status='hide'){
		if (!isset($id) || $id <=0 || $id == null) {
			return false;
		}
		if ($status=='hide'){
			$status = 'draft';
		}else if ($status=='show'){
			$status = 'publish';
		}else{
			$status = 'draft';
		}
		$sql_data_array = array('post_status' => $status);
		$updated = bw_perform(BW_TABLE_PREFIX."videos", $sql_data_array, 'update', 'id = \'' . $id . '\'');
		return $updated;
	}
	function bw_sort_order($id, $direction='up'){
		if ($id>0){
			global $wpdb;
			$sql = "SELECT * FROM ".BW_TABLE_PREFIX."videos where 1 order by sortorder, id";
			$results		= $wpdb->get_results($sql);
			$maxRowCount	= count($results);
			if($maxRowCount > 0){
				$rowCount = 0;
				$defaultOrder = $sortOrder = array();
				$mem = false;
				foreach($results as $result){
					$defaultOrder[] = $result->id;
					$sortOrder[] = $result->id;
					if ($direction=="up" && count($sortOrder)>1){
						if ($result->id==$id){
							$stack = $sortOrder[count($sortOrder)-2];
							$sortOrder[(count($sortOrder)-2)] = $id;
							$sortOrder[(count($sortOrder)-1)] = $stack;
						}
					}else if ($direction=="down") {
						if ($mem===false && $result->id==$id){
							$mem = $id;
						}else{
							if ($mem!==false){
								$sortOrder[(count($sortOrder)-2)] = $result->id;
								$sortOrder[(count($sortOrder)-1)] = $mem;
								$mem = false;
							}
						}
					}
					$rowCount++;
				}
				foreach ($sortOrder as $sort_order => $id){
					$sql_data_array = array('sortorder' => $sort_order);
					$updated = bw_perform(BW_TABLE_PREFIX."videos", $sql_data_array, 'update', 'id = \'' . $id . '\'');
				}
			}
		}
	}
	function bw_deleteRecord($id=null){
		global $wpdb;
		if (!isset($id) || $id <=0 || $id == null) {
			return false;
		}
		$sql = "DELETE FROM ".BW_TABLE_PREFIX."videos where  id='".$id."' limit 1";
		$res = $wpdb->query($sql);
		return $res;
	}
	function bw_show_listing($msg=false){
		global $wpdb;
		$sql = "SELECT * FROM ".BW_TABLE_PREFIX."videos where 1 order by sortorder, id";
		$results		= $wpdb->get_results($sql);
		$maxRowCount	= count($results);
		?>
		<h2>Blue Wrench Video Widget
		<a href="?page=bw-videos" class="add-new-h2">Add New Video</a></h2><?php echo $msg;?>
		<div class="clear">&nbsp;</div>
		<?php

		if($maxRowCount > 0){
			?>
			<script language="javascript">
			<!--
				function confirmDelete(){
					return confirm("Are you sure to delete this record?");
				}
			//-->
			</script>
			<table class="wp-list-table widefat fixed pages" id="bwv_listing" cellspacing="0">
			<thead>
				<tr>
					<th style="width:40px;">#</th>
					<th style="width:300px;">Title</th>
					<th>Video (URL)</th>
					<th style="width:60px;">Visibility</th>
					<th style="width:270px;">Actions</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th style="width:40px;">#</th>
					<th style="width:300px;">Title</th>
					<th>Video (URL)</th>
					<th style="width:60px;">Visibility</th>
					<th style="width:270px;">Actions</th>
				</tr>
			</tfoot>
			<tbody>
			<?php
				$rowCount = 0;
				$bwMediaController = new BlueWrenchMediaController();
				foreach($results as $result){
					//p($result);
					$rowCount++;
					$id = $result->id;
					$sName = $result->title;
					$value = $result->value;
					if ($result->post_status=="publish"){
						$visibility = "Shown";
					}else{
						$visibility = "Hidden";
					}
					//$video_preview = $bwMediaController->generateEmbeddHTML($value);
					$video_preview = "<div style='height:".$bwMediaController->videoheight."px; width:".$bwMediaController->videowidth."px'><img style='margin-top:".intval($bwMediaController->videoheight/2-11)."px; margin-left:".intval($bwMediaController->videowidth/2-11)."px;' src='".plugin_dir_url( __FILE__ ) . '/images/ajax-loader.gif'."'></div>";
					$sortorder = $result->sortorder;
					if ($visibility=='Shown'){
						$visibility_action = '<a href="?page=bluewrench-video-widget/bluewrench-video-widget.php&action=hide&id='.$id.'"><strong>Hide in Widget</strong></a>';
					}else{
						$visibility_action = '<a href="?page=bluewrench-video-widget/bluewrench-video-widget.php&action=show&id='.$id.'">Show in Widget</a>';
					}
					$sort_action_a = '<a class="bwv_sort_link_n" href="?page=bluewrench-video-widget/bluewrench-video-widget.php&action=moveup&id='.$id.'" title="Move Up"><span class="ui-icon ui-icon-arrowthick-1-n"></span></a>';
					$sort_action_b = '&nbsp;&nbsp;<a class="bwv_sort_link_s" href="?page=bluewrench-video-widget/bluewrench-video-widget.php&action=movedown&id='.$id.'" title="Move Down"><span class="ui-icon ui-icon-arrowthick-1-s"></span></a>';
					if ($rowCount==1){
						$sort_action_a = '';
					}
					if ($rowCount == $maxRowCount){
						$sort_action_b = '';
					}
					$sort_actions = '<div class="bwv_sort_actions">'.$sort_action_a.$sort_action_b.'</div>';


			?>
			   <tr>
				 <td><?php echo $rowCount; ?></td>
				 <td><?php echo $sName; ?></td>
				 <td><a href="javascript: void(0);" onclick='javascript: bw_slideToggle(<?php echo $id;?>);'><?php echo $value; ?></a><div class="bw_preview_container" id='prev_<?php echo $id;?>' style="display:none;"><?php echo $video_preview;?></div></td>
				 <td><?php echo $visibility; ?></td>

				 <td><a onclick="javascript: return confirmDelete();" href="?page=bluewrench-video-widget/bluewrench-video-widget.php&action=delete&id=<?php echo $id; ?>">Delete</a> <a href="?page=bw-videos&action=edit&id=<?php echo $id; ?>">Modify</a><?php echo $visibility_action.$sort_actions;?></td>
			   </tr>
			<?php
				}
			?>
			</table>
			<?php
		}else{
			echo ( __( '<div class="error"><h4>No Video found.<br /><br />Please click here to <a href="?page=bw-videos">Add</a> new video.</h4></div>' ) );
		}
	}
	function bw_update_video_info($title='', $url='', $mode='add', $fld='import'){
		global $wpdb;
		/**
		*	Error Codes
		*	 1 means everything is ok
		*	-1 means Invalid URL	
		*	-2 means duplicate URL
		*	-3 means internal error
		*	-4 means unsupported network
		**/

		$bw_title			= trim($_REQUEST['bw_title']);
		$bw_title			= substr($bw_title, 0, 255);

		$bw_url				= trim($_REQUEST['bw_url']);
		if ($bw_url==""){
			return -1;
		}

		$bwMediaController = new BlueWrenchMediaController();
		if (!$bwMediaController->is_supported_network($bw_url)){
			return -4;
		}


		if ($mode=='add'){
			$sortorder = 0;
			$sql_data_array = array('title' => bw_prepare_input($bw_title),
							  'value' => bw_prepare_input($bw_url),
							  'sortorder' => bw_prepare_input($sortorder),
							  'dateadded' => 'now()');

		}else{
			$sql_data_array = array('title' => bw_prepare_input($bw_title),
							  'value' => bw_prepare_input($bw_url),
							  'dateadded' => 'now()');
		}


		if ($mode=='edit' && (isset($_POST['bw_id']) && trim($_POST['bw_id']) > 0)){
			$bw_id = $_POST['bw_id'];
			$bw_current_file = $_POST['bw_current_file'];
		}else{
			$bw_id = 0;
		}
		if ($mode=='add'){
			$sql = "SELECT * FROM ".BW_TABLE_PREFIX."videos where  value='".$bw_url."' limit 1";
			$results = $wpdb->get_results($sql);
			if(count($results) > 0){
				return -2;
			}else{
				$updated = bw_perform(BW_TABLE_PREFIX."videos", $sql_data_array);
			}
		}else{
			$updated = bw_perform(BW_TABLE_PREFIX."videos", $sql_data_array, 'update', 'id = \'' . $bw_id . '\'');
		}
		if ( false === $updated ){ 
			return -3;
			//echo "Error: ".$fld."<br/>A problem occurred during file upload!<br />".$sql."<br />";
			//wp_die( __('ERROR: Invalid Query.') );
		}
		return 1;
	}

	function bw_show_form(){
		global $wpdb, $action;
		$hiddenElems = $bw_url = $bw_title = "";
		if ((isset($_GET['action']) && $_GET['action']=="edit") && (isset($_GET['id']) && $_GET['id']>0 ) ){
			$pageTitle = "Blue Wrench Video Widget - Modify Video";
			$currentId = $_GET['id'];
			$sql = "SELECT * FROM ".BW_TABLE_PREFIX."videos WHERE id='".$currentId."' LIMIT 1 ;";
			$res = $wpdb->query($sql);

			$results = $wpdb->get_results($sql);
			if(count($results) > 0){
				$bw_url = $results[0]->value;
				$bw_title = $results[0]->title;
				$bw_current_file = $results[0]->value;
				

				$hiddenElems = '<input type="hidden" name="bw_id" value="'.$currentId.'">';
				$hiddenElems .= "\n";
			}
			$mode = 'Modify';
		}else{
			$pageTitle = "Blue Wrench Video Widget - Add a new video";
			$currentId = 0;
			$mode = 'Add';
		}
		$msg = "";
		if ( (isset($_POST['action']) && trim($_POST['action']) == "save") && (isset($_POST['submit']) && ( trim($_POST['submit']) == "Add Video" || trim($_POST['submit']) == "Modify Video") )) {
			global $wpdb;
			$bw_title = $title	= $_POST['bw_title'];
			$bw_url = $url	= $_POST['bw_url'];
			$br_nonce  = $_POST['br_nonce'];
			//$br_nonce  = 'br_nonce_'.BW_VERSION_NUM
			if(!wp_verify_nonce($br_nonce, 'br_nonce_'.BW_VERSION_NUM)) die("No CSRF for you!");

			if ((isset($_POST['bw_id']) && trim($_POST['bw_id']) > 0)){ 
				/*Modify*/
				$result = bw_update_video_info($title, $url, 'edit');
				if ($result==1){
					$url = "?page=bluewrench-video-widget/bluewrench-video-widget.php&msg=modified";
					wp_redirect($url);
				}else{
					global $bw_error_messages;
					$msg =  '<div class="error"><h4>'.$bw_error_messages[$result].'</h4></div>' ;
				}
			}else{
				$result = bw_update_video_info($title, $url);
				if ($result==1){
					$url = "?page=bluewrench-video-widget/bluewrench-video-widget.php&msg=added";
					wp_redirect($url);
				}else{
					global $bw_error_messages;
					$msg =  '<div class="error"><h4>'.$bw_error_messages[$result].'</h4></div>' ;
				}
			}
		}
		$br_nonce = wp_create_nonce( 'br_nonce_'.BW_VERSION_NUM );
?>
<h2><?php echo $pageTitle;?></h2>
<?php echo $msg;?>
<form id="upload-form" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class="form-table">
		<tr valign="top">
			<th scope="row">Title</th>
			<td>
				<input id="bw_title" maxlength="75" size="70" name="bw_title" value="<?php echo $bw_title;?>" /> 
			</td>
		</tr>
		<tr valign="top ">
			<th scope="row">URL</th> 
			<td>
				<input id="bw_url" maxlength="255" size="70" name="bw_url" value="<?php echo $bw_url;?>" />
				<p>Hint: Simply copy the video's URL from your web browser's address bar while viewing the video</p>

				<a id="bw_infobar_link" href="javascript: void(0);" onclick="javascript: bw_infobarToggle();">More Help</a>

				<div id="bw_infobar" style="display:none;">
				<p>Examples:<br />http://www.youtube.com/embed/XXXXXXXXXXX<br />http://player.vimeo.com/video/XXXXXXXX<br />http://www.dailymotion.com/video/XXXXXXX</p>

				<p><strong>Note for Yahoo! Video: </strong>Please use Embedd button on video player to get the video link. See screenshot below:</p>
				<img src='<?php echo plugin_dir_url( __FILE__ ) . '/images/yahoo.png' ?>'>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" name="submit" id="submit" class="button-primary" value="<?php echo $mode;?> Video">
				<input type="hidden" name="action" value="save">
				<?php echo $hiddenElems;?>
			</td>
		</tr>
	</table>
	<input type="hidden" name="br_nonce" value="<?php echo $br_nonce;?>">
</form>
<?php
	}	// function bw_show_form ends

	$showListing = true;
	if (isset($_REQUEST['page']) && $_REQUEST['page'] == "bw-videos") {
		$showListing = false;
	}

	if ((isset($_GET['action']) && $_GET['action'] == "delete") && $_GET['id']>0 ) {
		$id = $_GET['id'];
		bw_deleteRecord($id);
		$url = "?page=bluewrench-video-widget/bluewrench-video-widget.php&msg=deleted";
		wp_redirect($url);
	}else if ((isset($_GET['action']) && $_GET['action'] == "hide") && $_GET['id']>0 ) {
		$id = $_GET['id'];
		bw_set_status ($id,'hide');
		$url = "?page=bluewrench-video-widget/bluewrench-video-widget.php&msg=updated";
		wp_redirect($url);
	}else if ((isset($_GET['action']) && $_GET['action'] == "show") && $_GET['id']>0 ) {
		$id = $_GET['id'];
		bw_set_status($id,'show');
		$url = "?page=bluewrench-video-widget/bluewrench-video-widget.php&msg=updated";
		wp_redirect($url);
	}else if ((isset($_GET['action']) && $_GET['action'] == "moveup") && $_GET['id']>0 ) {
		$url = "?page=bluewrench-video-widget/bluewrench-video-widget.php";
		$id = $_GET['id'];
		bw_sort_order($id,'up');
		wp_redirect($url);
	}else if ((isset($_GET['action']) && $_GET['action'] == "movedown") && $_GET['id']>0 ) {
		$url = "?page=bluewrench-video-widget/bluewrench-video-widget.php";
		$id = $_GET['id'];
		bw_sort_order($id,'down');
		wp_redirect($url);
	}else{

	}
	?>
	<div class="wrap">
	<?php
		$msg = '';
		if ((isset($_GET['msg']) && $_GET['msg'] != "")) {
			switch ($_GET['msg']){
				case 'deleted':
					$msg .= "<div class='updated'><p>Record deleted sucessfully.</p></div>";
					$msg .= '<div class="clear"></div>';
					break;
				case 'added':
					$msg .= "<div class='updated'><p>Record sucessfully added.</p></div>";
					$msg .= '<div class="clear"></div>';
					break;
				case 'modified':
				case 'updated':
					$msg .= "<div class='updated'><p>Record modified sucessfully.</p></div>";
					$msg .= '<div class="clear"></div>';
					break;
				default:
					$msg .= "<div class='updated'><p>".$_GET['msg']."</p></div>";
					$msg .='<div class="clear"></div>';
					break;
			}
		}
	?>
	<div id="icon-upload" class="icon32"></div>
	

	<?php
	if ($showListing){
		bw_show_listing($msg);
	}else{
		bw_show_form();
	}
	?>
	<div class="clear"></div>
	<?php
?>
 </div>