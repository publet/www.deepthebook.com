'use strict';

var $home = (function () {
    
    function animate () {
        /* original sequence with slider 
        $('#home-logo-1').fadeIn(3000, function() {
            $('#home-logo-2').fadeIn(1000, function() {
                var loader = $('<div id="home-loader" style="display:none;"><div id="home-loader-inner"><p>Loading...</p></div></div>').appendTo('body').fadeIn(300);
                setTimeout(function() {
                    $(loader).fadeOut(300, function() {
                        $('#home-sequence').animate({top:'75px'}, 600, function() {
                            $('#home-slider').fadeIn(1000, function() {
                                $('#snow-canvas').fadeOut(5000);
                            });
                        });
                    });
                }, 2000);
            });
            $('#home-title-inner').fadeIn(1000);
        });
        */

        var loader = $('<div id="home-loader" style="display:none;"><div id="home-loader-inner"><p>Loading...</p></div></div>').appendTo('body').fadeIn(300, function () {
            setTimeout(function() {
                $(loader).fadeOut(300, function() {
                    $('#home-logo-1').fadeIn(3000, function() {
                        $('#home-logo-2').fadeIn(1000, function () {
                            $('#home-title-inner').fadeIn(1000);
                        });
                    });
                });
            }, 500);
        });
    }

    return {
        animate: animate
    };

}());

$(document).ready(function() {
    var img = new Image();
    img.src = 'img/ice-bg.png';
    img.onload = function () {
        $home.animate();
    };
});

// Angular app.
var app = angular.module('Deep', function() {});

// Filters.
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    };
});

// Services.
app.factory('PostStorage', function($http) {
    return {
        getPosts: function() {
            var promise = $http.jsonp('http://64.111.120.165/blog/?json=get_recent_posts&callback=JSON_CALLBACK');
            return promise;
        }
    };
});

// Controllers.
app.controller('PostsController', function($scope, PostStorage) {
    PostStorage.getPosts().then(function(response) {

        $scope.currentPage = 0;
        $scope.pageSize = 5;
        $scope.posts = response.data.posts;

        $scope.numberOfPages = function() {
            return Math.ceil($scope.posts.length / $scope.pageSize);                
        };
        
    });
});

window.app = app;
